#!/bin/bash

DESTINATION=$1

# clone Flectra directory
git clone --depth=1 https://gitlab.com/6ministers/business-apps/production-flectra3-postgres-nginx-ssl $DESTINATION
rm -rf $DESTINATION/.git

# set permission
mkdir -p $DESTINATION/postgresql
sudo chmod -R 777 $DESTINATION

