

# Installing FlectraHQ 3.0  Docker Compose. For production. 

## Functionality: Production-Flectra3+Postgres+Nginx+SSL (Certbot)

1. Ubuntu 22.04 operating system
2. Quick installation and configuration of Docker Composer (setup.sh )
3. Nginx web server
4. Cerbot SSL Let's Encrypt
5. Saving data in Volumes
6. Creating a database user for connecting BI (user-read-only.sh )
7. Installing packages of additional Python packages (entrypoint.sh )
8.  Updating the FlectraHQ 3 container
   


## Quick Installation

Before installation, you need to create one domain and two subdomains and send them to DNS on IP servers:
1. `flectra-domain.com` (domain)


Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/production-flectra3-postgres-nginx-ssl/-/raw/master/setup.sh | sudo bash -s
```

Download Flectra 3.0 instance:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/production-flectra3-postgres-nginx-ssl/-/raw/master/download.sh | sudo bash -s flectra3.0
```


If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd flectra3.0
```

# Docker compose
Настройка дdocker compose

``` bash
version: '3.9'

services:

  flectra:
    container_name: flectra3.0  
    image: flectrahq/flectra:3.0
    user: flectra
    restart: always             # run as a service 
    ports:
      - 7073:7073
      - 7072:7072 # live chat
    tty: true
    command: --
    environment:
      - HOST=db
      - PORT=5432     
      - USER=flectra
      - PASSWORD=password
    volumes:
      - ./etc/config:/var/lib/flectra:rw
      - ./etc/addons:/mnt/extra-addons:ro
      - ./etc/logs:/var/log/flectra:rw
      # - ./etc/timezone:/etc/timezone:ro
      # - ./etc/localtime:/etc/localtime:ro     
      # - ./etc/geoip-db:/usr/share/GeoIP:ro
      # - ./entrypoint.sh:/entrypoint.sh   # if you want to install additional Python packages, uncomment this line!
    depends_on:
      - db
    hostname: Flectra

  db:
    container_name: flectra-psql  
    image: postgres:16
    restart: always             # run as a service    
    environment:
      POSTGRES_USER: flectra
      POSTGRES_PASSWORD: password
      POSTGRES_DB: postgres
    volumes:
        - ./postgresql:/var/lib/postgresql/data
    hostname: flectradb

  nginx:
    container_name: nginx
    image: nginx:latest
    restart: always
    ports:
      - 80:80
      - 443:443
    volumes:
      - ./nginx/log:/var/log/nginx:rw
      - ./nginx/conf:/etc/nginx/conf.d/:ro
      - ./certbot/conf:/etc/nginx/ssl/:ro
      - ./certbot/www:/var/www/certbot/:ro
  
  certbot:
    container_name: certbot
    image: certbot/certbot:latest
    command: certonly --webroot --webroot-path=/var/www/html --email youremail@mail.com --agree-tos --no-eff-email -d domain.com -d www.domain.com
    volumes:
      - ./certbot/conf:/etc/letsencrypt/:rw
      - ./certbot/www:/var/www/certbot/:rw
      - ./certbot/logs:/var/log/letsencrypt/:rw
```


# SSL - HTTPS Cerbot

Specify the domain and email

``` bash
command: certonly --webroot --webroot-path=/var/www/html --email youremail@mail.com --agree-tos --no-eff-email -d domain.com -d www.domain.com
```

To update the certificate, use the command

``` bash
docker compose run --rm certbot renew
```

# Nginx Web Server
Configuring the web server configuration. Logs and configurations

Nginx Configuration
`/root/flectra3.0/nginx/conf`

Logs nginx
`/root/flectra3.0/nginx/log`

Logs certbot
`/root/flectra3.0/certbot/logs`

Logs Flectra
`/root/flectra3.0/nginx/log/flectra.error.log`

The SSL certificate. Nginx uses it. Everything is already set up to use it. Do not touch this directory
`/root/flectra3.0/certbot/conf/live`


# HTTPS - by default

Specify the domain and email

``` bash
    server_name domain.com;
```

``` bash
    server_name www.domain.com;
```

Replace it with your domain `domain.com`

``` bash
    ssl_certificate /etc/nginx/ssl/live/domain.com/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/domain.com/privkey.pem;
```

``` bash
map $http_upgrade $proxy_connection {
  default upgrade;
  '' close;
}

upstream flectra {
    server flectra:7073;
}

upstream flectra-chat {
    server flectra:7072;
}

server {
    listen [::]:80;
    listen 80;

    server_name domain.com www.domain.com;

    return 301 https://domain.com$request_uri;
}

server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;

    server_name www.flectrahq.ru;

    ssl_certificate /etc/nginx/ssl/live/domain.com/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/domain.com/privkey.pem;

    return 301 https://domain.com$request_uri;
}

server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;

    server_name domain.com;

    access_log  /var/log/nginx/flectra.access.log;
    error_log   /var/log/nginx/flectra.error.log;

    ssl_certificate /etc/nginx/ssl/live/domain.com/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/domain.com/privkey.pem;


    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/certbot;
    }

    location /longpolling {
    	 proxy_pass    http://flectra-chat;
    }

    location / {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header X-Forwarded-Host $host;
		proxy_set_header X-Forwarded-Proto https;
        proxy_pass http://flectra;
    }
 
    location ~* /web/static/ {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header X-Forwarded-Host $host;
		proxy_set_header X-Forwarded-Proto https;
        proxy_pass http://flectra;
    }

   location ~* /web/database/manager {
      # allow 121.121.121.121;
      # deny all;
        proxy_pass http://flectra;
   }

   location ~* /web/database/selector {
      # allow 121.121.121.121;
      # deny all;
        proxy_pass http://flectra;
   }

   gzip_types text/css text/scss text/plain text/xml application/xml application/json application/javascript;
   gzip on;
}
```

Restart NGINX
To accept the changes, use the command

``` bash
docker-compose restart nginx
```


# HTTP варинт настройки

Файл 
``` bash
default.conf_http
```

If you want to use it, replace the contents in the file

``` bash
default.conf
```

Каталог
``` bash
default.conf
```

``` bash
map $http_upgrade $proxy_connection {
  default upgrade;
  '' close;
}

upstream flectra {
    server flectra:7073;
}

upstream flectra-chat {
    server flectra:7072;
}

server {
    listen [::]:80;
    listen 80;
    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/certbot;
    }
	
    location /longpolling {
    	 proxy_pass    http://flectra-chat;
    }
	
    location / {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_pass http://flectra;
    }
    location ~* /web/static/ {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        proxy_pass http://flectra;
    }

   gzip_types text/css text/scss text/plain text/xml application/xml application/json application/javascript;
   gzip on;
}

```


# Run Flectra:

``` bash
docker-compose up -d
```


## Usage

- **If you get any permission issues**, change the folder permission to make sure that the container is able to access the directory:

``` sh
$ sudo chmod -R 777 addons
$ sudo chmod -R 777 etc
$ sudo chmod -R 777 postgresql
```


- To run Flectra container in detached mode (be able to close terminal without stopping Flectra):

```
docker-compose up -d
```

- To Use a restart policy, i.e. configure the restart policy for a container, change the value related to **restart** key in **docker-compose.yml** file to one of the following:
   - `no` =	Do not automatically restart the container. (the default)
   - `on-failure[:max-retries]` =	Restart the container if it exits due to an error, which manifests as a non-zero exit code. Optionally, limit the number of times the Docker daemon attempts to restart the container using the :max-retries option.
  - `always` =	Always restart the container if it stops. If it is manually stopped, it is restarted only when Docker daemon restarts or the container itself is manually restarted. (See the second bullet listed in restart policy details)
  - `unless-stopped`	= Similar to always, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts.
```
 restart: always             # run as a service
```

## Custom addons

The **addons/** folder contains custom addons. Just put your custom addons if you have any.
 

## Flectra container management

**Run Flectra**:

``` bash
docker-compose up -d
```

**Restart Flectra**:

``` bash
docker-compose restart
```

**Stop Flectra**:

``` bash
docker-compose down
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

Go to the catalog
``` bash
cd flectra3.0
```

Flectra Update

``` bash
cd flectra3.0
```
``` bash
sudo docker-compose down
```

``` bash
docker pull flectrahq/flectra:3.0
```
``` bash
sudo docker-compose up -d
```

## Live chat

# Файл конфигурации Flectra

``` bash
/root/flectra3.0/etc/config/flectra.conf
```


## docker-compose.yml

* flectrahq/flectra:3.0
* postgres:16


# Documentation

